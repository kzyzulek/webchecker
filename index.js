const express = require('express')
const axios = require('axios');
const app = express()
//const port = 3002
const Website = require('./models/websites');
const Visit = require('./models/visits');

const port = process.env.PORT || 3002;

const mongoose = require('mongoose');
const moment = require('moment');
const CronJob = require('cron').CronJob;

mongoose.connect('mongodb+srv://kzyz:QmJmfcIPbWXneAIn@cluster0.t1coz.mongodb.net/webchecker');


app.post('/fetchers', async (req, res) => {

    try {
        let inDatabase = await Website.findOne({ url: req.query.url })
        if(inDatabase.url)
        {
            res.send({result: "failed",message:"url exists"})
            return
        }
        let website = new Website({
            url: req.query.url,
            visit_interval: req.query.visit_interval //in minutes
        });
        website = await website.save();

        let websites = await Website.find({url:req.query.url}).select();

    for (let i = 0; i < websites.length; i++) {
        let website = websites[i]

        await scheduleJob(website)


    }
        let response = {
            result: "success"
        }

        res.send(response)

    } catch (error) {

        res.send({result: "error", error: error})
    }
})


app.get('/test', async (req, res) => {


    let oczekiwany_wyraz=38

    //fibonacci(oczekiwany_wyraz)
    

    function fibonacci(num) {
        if (num <= 1) return 1;
      
        return fibonacci(num - 1) + fibonacci(num - 2);
      }




res.send({log:fibonacci(oczekiwany_wyraz)})
})

app.get('/loaderio-0c2f62eab79546d24df239172e3429af/', async (req, res) => {


    res.send('loaderio-0c2f62eab79546d24df239172e3429af')
    })


app.get('/fetchers', async (req, res) => {


    if(req.query.uuid)
    {
        let website = await Website.findOne({ _id: req.query.uuid })

        if(website){
            visits = await Visit.find({website_id:website})

            res.send(visits)
            return
        }else
        {
            res.send({result:"failed",message:"url not in database"})
        }
 

    } else
    {
        let websites = await Website.find({}).select();
        res.send(websites)
    }
})


app.delete('/fetchers', async (req, res) => {

    let website = await Website.findByIdAndRemove(req.query.uuid)
    
    let visits = await Visit.deleteMany({'website_id':req.query.uuid}).select();

    res.send({result: "success"})

})


async function start_cron()
{
    let websites = await Website.find({}).select();

    for (let i = 0; i < websites.length; i++) {
        let website = websites[i]

        await scheduleJob(website)


    }

}

function scheduleJob(website) {
    let job = new CronJob('*/' + website.visit_interval + ' * * * * *', async function () {

        const url = website.url;
        try {
            await axios(url)
                .then(response => {
                    const html = response.data;


                    if(response.data==null)
                    {
                        let visit2 = new Visit({
                            response: null,
                            visit_time: moment().format("DD MM YYYY hh:mm:ss"),
                            website_id: website._id //in minutes    
                        })
                        visit2 = visit2.save()
                    }

                    let visit = new Visit({
                        response: "works",
                        visit_time: moment().format("DD MM YYYY hh:mm:ss"),
                        website_id: website.id //in minutes
                    });
                    visit = visit.save()
                })
        } catch (error) {
            let visit2 = new Visit({
                response: null,
                visit_time: moment().format("DD MM YYYY hh:mm:ss"),
                website_id: website.id //in minutes    
            })
           visit2.save()
        }

        console.log("works " + url)

    })

    job.start();

}


app.listen(port, () => {
    start_cron()
    console.log(`Example app listening at http://localhost:${port}`)
})