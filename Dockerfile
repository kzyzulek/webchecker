FROM node:14.15.5-alpine3.10
#From Biblioteka: Konkretna wersja
ENV NODE_ENV=production

WORKDIR /


COPY ["package.json", "package-lock.json*", "./"]


RUN npm install --production

# EXPOSE 3002:3002

COPY ["index.js","./"]

COPY ["models","./models"]


CMD [ "node", "index.js" ]

