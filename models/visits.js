var mongoose = require('mongoose');

const Visits =  new mongoose.model('visit', new mongoose.Schema({
    //url: { type: String },
    response:{type: String},
    visit_time: {type: String},
    website_id: { type: mongoose.Schema.Types.ObjectId, ref: 'websites' }
},
    {
        timestamps:
        {
            createdAt: 'created_at'
        }
    }
), 'visit')

module.exports = Visits;
