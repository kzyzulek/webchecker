var mongoose = require('mongoose');

const Websites =  new mongoose.model('websites', new mongoose.Schema({
    url: { type: String },
    visit_interval: {type: Number},
},
    {
        timestamps:
        {
            createdAt: 'created_at'
        }
    }
), 'websites')


module.exports = Websites;

// const OrderPerson = mongoose.model('order_people', new mongoose.Schema({
//     status: { type: String },
//     username: { type: String },
//     order_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Order', required: false },
//     user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
//     party_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Party', required: false },
//     payment_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Payment', required: false },
//     party_person_id: { type: mongoose.Schema.Types.ObjectId, ref: 'PartyPerson', required: false }
// },
//     {
//         timestamps:
//         {
//             createdAt: 'created_at'
//         }
//     }
// ), 'order_people')